var images = {

	image0: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/eingang1920x1280.jpg",
		p1000: "./imgs/gallery/eingang1001x671.jpg",
		p740: "./imgs/gallery/eingang1920x1280.jpg"
	},
	image1: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/karte1920x1280.jpg",
		p1000: "./imgs/gallery/karte1001x667.jpg",
		p740: "./imgs/gallery/karte1920x1280.jpg"
	},
	image2: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/kontakt1920x1280.jpg",
		p1000: "./imgs/gallery/kontakt1001x667.jpg",
		p740: "./imgs/gallery/kontakt1920x1280.jpg"
	},
	image3: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/leinwand1920x1256.jpg",
		p1000: "./imgs/gallery/leinwand1001x655.jpg",
		p740: "./imgs/gallery/leinwand1920x1256.jpg"
	},
	image4: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/mercedes2-1920x1280.jpg",
		p1000: "./imgs/gallery/mercedes2-1001x667.jpg",
		p740: "./imgs/gallery/mercedes2-1920x1280.jpg"
	},
	image5: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/mercedes3-1920x1280.jpg",
		p1000: "./imgs/gallery/mercedes3-1001x667.jpg",
		p740: "./imgs/gallery/mercedes3-1920x1280.jpg"
	},
	image6: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/mercedes1920x1280.jpg",
		p1000: "./imgs/gallery/mercedes1001x667.jpg",
		p740: "./imgs/gallery/mercedes1920x1280.jpg"
	},
	image7: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/unterricht2-1920x1280.jpg",
		p1000: "./imgs/gallery/unterricht2-1001x667.jpg",
		p740: "./imgs/gallery/unterricht2-1920x1280.jpg"
	},
	image8: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/unterricht3-1920x1280.jpg",
		p1000: "./imgs/gallery/unterricht3-1001x667.jpg",
		p740: "./imgs/gallery/unterricht3-1920x1280.jpg"
	},
	image9: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/unterricht4-1920x1280.jpg",
		p1000: "./imgs/gallery/unterricht4-1001x667.jpg",
		p740: "./imgs/gallery/unterricht4-1920x1280.jpg"
	},
	image10: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/unterricht-1920x1280.jpg",
		p1000: "./imgs/gallery/unterricht-1001x667.jpg",
		p740: "./imgs/gallery/unterricht-1920x1280.jpg"
	},
	image11: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/unterrichtsraum1920x1280.jpg",
		p1000: "./imgs/gallery/unterrichtsraum1001x667.jpg",
		p740: "./imgs/gallery/unterrichtsraum1920x1280.jpg"
	},
	image12: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/unterrichtsraumHinten1920x1280.jpg",
		p1000: "./imgs/gallery/unterrichtsraumHinten1001x667.jpg",
		p740: "./imgs/gallery/unterrichtsraumHinten1920x1280.jpg"
	},
	image13: {
		title: "Guenters Fahrschulen",
		path: "./imgs/gallery/unterrichtsraumZoom1920x1280.jpg",
		p1000: "./imgs/gallery/unterrichtsraumZoom1001x667.jpg",
		p740: "./imgs/gallery/unterrichtsraumZoom1920x1280.jpg"
	} // <---- Beim letzten kein Komma
};
