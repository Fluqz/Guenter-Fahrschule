$(function(){
	'use: strict';
	$window = $(window);	// window
	$owl = $("#owl");
	var wrapper = document.querySelector('#wrapper');	// General wrapper
	var openMenu = document.querySelector('.toggle-menu');
	var listItems = document.querySelectorAll('.menu .menu-icons');
	var sections = document.querySelectorAll('.section-to-scroll');
	var menu = document.querySelector('.menu');
	var bluebar = document.querySelector('.blue-side');
	var menuIsOpen = false;
	var mapSwitcher = document.querySelectorAll('.map-switcher');
	var socials = document.querySelector('.header-social');
	var socialIcon = document.querySelector('.show-socials');
	var mapTouch = document.querySelector('.map-touch');
	var mapLock = document.querySelector('.lock-map');
	$scrollInfo = $('#scroll-info');
	$innerWrapper = $('#inner-wrapper');
	var tabs = document.querySelectorAll('.menu-text');
	var screenRatio = window.devicePixelRatio || 1;
	var screenSizeElements = document.querySelectorAll('.screen-height');
	var isMobile = false;
	var mapOptions;
	var mqs = {
		path: 'path',
		p1000: 1001,
		p740: 780
	};

	// Mobile only JavaScript
	var isMobileDevice = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobileDevice.Android() || isMobileDevice.BlackBerry() || isMobileDevice.iOS() || isMobileDevice.Opera() || isMobileDevice.Windows());
    }
	};

	// called on Desktop or Mobile
	if(mqs.p1000 >= (window.screen.width * screenRatio) || isMobileDevice.any())
		mobileJavaScript();
	else desktopJavaScript();

	// JS for mobile
	function mobileJavaScript(){
		console.log('Mobile? :');
		console.log(isMobile);


		isMobile = true;
		$('.education-single').css({'width': '100%', 'display': 'flex'});
		$(".education-content").owlCarousel({
				items: 1,
				autoplay: true,
				loop: true,
				center: true,
				nav: false,
				autoplayHoverPause: true,
				autoplaySpeed: 600,
				autoplayTimeout: 5000,
				dots: true
		});
		for(var i=0;i<screenSizeElements.length;i++)
			setToScreenHeight(screenSizeElements[i]);
	}

	// JS for desktop
	function desktopJavaScript(){
			isMobile = false;
			$('.education-single').css({'width': '33.3333%', 'display': 'flex'});
			var linkToMaps = document.querySelector('.link-to-maps a');
			linkToMaps.setAttribute('target', '_blank');
			linkToMaps.setAttribute('title', 'Open new window');
			
		console.log('Mobile? :');
		console.log(isMobile);
	}



	// on resize
	$window.resize(function(event){
		wrapper.style.width = '100%';
		wrapper.style.transition = 'width .0001s';
		closeMenu();
		unloadCrumbs();
	});


	// Owl Carousel
	initOwlImages(initGalleryOwl);
	// Creates html for images in #owl
	function initOwlImages(initGalleryOwl){
		var dimensions = getBrowserSize();
		var mobileSize = getMobileImageSize(dimensions.width);
		var count = getGalleryImagesSize();
		var galleryOwl = document.querySelector('#owl');

		for(var i=0; i<count;i++){
			var galleryDiv = document.createElement('div');
			galleryDiv.classList.add('item');

			galleryDiv.style.width = "100%;";

			if((mqs[mobileSize] >= (dimensions.width * screenRatio) && isMobile) && (images['image'+i][mobileSize] !== "")){
					galleryDiv.style.backgroundImage = "url('"+images['image'+i][mobileSize]+"')";
					galleryDiv.setAttribute('title', images['image'+i].title);
			}
			else{
				if(images['image'+i][mobileSize] !== ""){
					galleryDiv.style.backgroundImage = "url('"+images['image'+i].path+"')";
					galleryDiv.setAttribute('title', images['image'+i].title);
				}
				else console.log('No Path for images');
			}

			galleryOwl.appendChild(galleryDiv);
		}
		// if imgs are loaded
		imagesLoaded(wrapper, function(){
			imagesLoaded(document.querySelector('#owl'), function(){
				$('#load-screen').removeClass('visible');
			});
		});
		// Callback
		initGalleryOwl();
	}

	// Gets number of images in images.js
	function getGalleryImagesSize(){
		var size = 0;
		for(var key in images){
			if(images.hasOwnProperty(key))
				size++;
		}
		return size;
	}

	// init Gallery with owl carousel
	function initGalleryOwl(){
		$owl.owlCarousel({
				items: 1,
				autoplay: true,
				center: true,
				nav: false,
				autoplayHoverPause: true,
				autoplaySpeed: 600,
				autoplayTimeout: 50000,
				dots: true,
				loop: true
		});
	}
	$owl.on('changed.owl.carousel', function(event){
		console.log(event);
	});

	var owlNavBtns = document.querySelectorAll('.owl-nav-btn');

	for(var i=0; i<owlNavBtns.length; i++)
		owlNavBtns[i].addEventListener('click', nextPrevImage);

	function nextPrevImage(event){
		if(event.target.classList.contains('owl-nav-next')){
			$owl.trigger('next.owl.carousel');
		}
		else if(event.target.classList.contains('owl-nav-prev')){
			$owl.trigger('prev.owl.carousel');
		}
	}

	// get screen size  desktop=path;1000px=p1000;780px=p780
	function getMobileImageSize(screenSize){
		if(screenSize > mqs.p1000)
			return 'path';
		else if(screenSize < mqs.p1000 && screenSize > mqs.p740)
			return 'p1000';
		else return 'p740';
	}

	/* Smoothscroll all links */
	$('.smooth-scroll').smoothScroll({easing: 'swing'});

	/* Toggle menu */
	openMenu.addEventListener('click', slideOutMenu, false);
	// Slides out menu and shrinks wrapper
	function slideOutMenu(){
			menu.classList.toggle('menu-visible');
			openMenu.classList.toggle('open-menu');
			if(menuIsOpen){
				wrapper.style.width = '100%';
				menuIsOpen = false;
			}
			else{
				wrapper.style.width = (wrapper.offsetWidth-150) + 'px';
				menuIsOpen = true;
			}
			wrapper.style.transition = 'width .3s cubic-bezier(.74,.19,.69,.85)';
			google.maps.event.trigger(map, 'resize');
	}

	// Determines if clicked on Menu/Social/MenuIcons
	$(this).mouseup(function(event){
		var menuF = $('.menu');
		var socialF = $('.header-social');
		var socialBtnF = $('.show-socials.menu-icons');
		if(!menuF.is(event.target) && menuF.has(event.target).length === 0 && menuIsOpen)
			closeMenu();

		if(!socialF.is(event.target) && socialF.has(event.target).length === 0 &&
			!socialBtnF.is(event.target) && socialBtnF.has(event.target).length === 0){
			socials.classList.remove('social-visible');
			socialIcon.children[0].classList.remove('focus-social');
		}
	});

	// Closes the menu
	function closeMenu(){
		menu.classList.remove('menu-visible');
		openMenu.classList.remove('open-menu');
		wrapper.style.width = '100%';
		menuIsOpen = false;

		wrapper.addEventListener('transitionend', function(){
			google.maps.event.trigger(map, 'resize');
		});
	}

	// On Scrolling fires event
	$window.scroll(function(event) {
		loadCrumbs();
		if(isMobileDevice.any()){
			setMapBlock();
			fixMapLocker();
		}
	});

	// Fires event on load
	$window.load(function() {
		unloadCrumbs();
		loadCrumbs();
	});

	// Checks menu for breadcrumps
	function loadCrumbs(){
		for(var i = 0; i < listItems.length-1; i++){
			if(sections[i].offsetTop-1 <= $(this).scrollTop() && !hasClass(sections[i], 'already-scrolled')){

				listItems[i].classList.add('already-scrolled');

				if(listItems[listItems.length-2].classList.contains('already-scrolled'))
					bluebar.classList.add('filled-blue');
				else if(bluebar.classList.contains('filled-blue'))
					bluebar.classList.remove('filled-blue');
			}
			else{
				listItems[i].classList.remove('already-scrolled');
				bluebar.classList.remove('filled-blue'); return;
			}
			if(i == 5)
				socialIcon.children[0].classList.add('fill-social');
			else
				socialIcon.children[0].classList.remove('fill-social');
		}
	}

	// deletes breadcrumps
	function unloadCrumbs(){
		for(var i = 0; i < listItems.length-1; i++)
			listItems[i].classList.remove('already-scrolled');
		socialIcon.classList.remove('fill-social');
	}


	// Show Social Media
	socialIcon.addEventListener('click', slideOutSocial, false);

	function slideOutSocial(){
		socials.classList.toggle('social-visible');
		socialIcon.children[0].classList.toggle('focus-social');
	}


	// GOOGLE MAPS */
	var map;
	var isActive;
	var link = ["https://www.google.de/maps/place/G%C3%BCnter+Schaal+Fahrschule/@48.6242885,8.7461661,16z/data=!4m8!1m2!2m1!1sg%C3%BCnters+fahrschulen+wildberg!3m4!1s0x47975b5ef6b1ffdf:0x3bd0f5b7f30ff685!8m2!3d48.6249042!4d8.747736", "https://www.google.de/maps/place/Inh.G%C3%BCnter+Schaal+G%C3%BCnters+Fahrschule/@48.66511,8.6855013,17z/data=!3m1!4b1!4m5!3m4!1s0x4797430787e172a3:0xdb9df5b8512f7b6d!8m2!3d48.66511!4d8.68769"];
	google.maps.event.addDomListener(window, 'load', initialize);
	function initialize(){
		var locations = document.querySelectorAll('.map-switcher');
		var lat = [48.624920, 48.665152];
		var lng = [8.747755, 8.687720];
		var title = ['Günters Fahrschule Wildberg', 'Günters Fahrschule Neubulach'];
		var text = ['<strong>Günters Fahrschule</strong><br/>Talstraße 26<br/>72218 Wildberg',
					'<strong>Günters Fahrschule</strong><br/>Calwer Straße 53<br/>75387 Neubulach'];

		var mapCanvas = document.getElementById('map-canvas');

		for(var i = 0; i < locations.length; i++){
			if(hasClass(locations[i], 'active')){
				mapOptions = switchLocation(lat[i], lng[i]);
				isActive = i;
				console.log(isActive);
				break;
			}
		}

			map = new google.maps.Map(mapCanvas, mapOptions);

		var infoWindow = new google.maps.InfoWindow({content: 'loading'});

		var marker = new google.maps.Marker({
			map: map,
			animation: google.maps.Animation.Drop,
			title: title[isActive],
			position: new google.maps.LatLng(lat[isActive], lng[isActive]),
			html: text[isActive]
		});
		infoWindow.setContent(marker.html);
		google.maps.event.addListener(marker, 'click', function(){
			infoWindow.open(map, this);
		});
		infoWindow.open(map, marker);
	}

	function switchLocation(lat, lng){
		 mapOptions = {
			center: new google.maps.LatLng(lat, lng),
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		return mapOptions;
	}

	var mapLink = document.querySelector('.link-to-maps');
	document.querySelector('.link-to-maps a').href = link[1];
	mapLink.addEventListener('click', function(){
		if(isActive === 0)
			document.querySelector('.link-to-maps a').href = link[0];
		else document.querySelector('.link-to-maps a').href = link[1];
	});




	for(var i = 0; i < mapSwitcher.length; i++)
		mapSwitcher[i].addEventListener('click', switchActiveMap, false);

	function switchActiveMap(event){
		for(var i = 0; i < mapSwitcher.length; i++){
			if(hasClass(mapSwitcher[i], 'active')){
				mapSwitcher[i].classList.remove('active');
			}
			else
				mapSwitcher[i].classList.add('active');
		}
		initialize();
	}

	mapTouch.addEventListener('click', toggleMapTouch, false);
	mapLock.addEventListener('click', toggleMapTouch, false);

	function toggleMapTouch(){
		mapTouch.classList.toggle('block-map');
		mapLock.childNodes[0].classList.toggle('fa-lock');
		mapLock.childNodes[0].classList.toggle('fa-unlock');
	}
	function setMapBlock(){
		mapTouch.classList.add('block-map');
		mapLock.childNodes[0].classList.add('fa-lock');
		mapLock.childNodes[0].classList.remove('fa-unlock');
	}

	if(isMobileDevice.any())
		fixMapLocker();

	function fixMapLocker(){
		var mapPos = document.querySelector('#map-canvas').getBoundingClientRect();
		var height = window.innerHeight;
		if(mapPos.top < 0 && mapPos.bottom > 75){
			mapLock.classList.add('lock-map-fixed');
			mapLock.classList.remove('lock-map-absolute');
			console.log(1);
		}
		else if(mapPos.top > 0 && mapPos.bottom > 100){
			mapLock.classList.remove('lock-map-fixed');
			console.log(2);
		}
		else if(mapPos.top > -height && mapPos.bottom > 55){
			console.log(3);
		}
		else if(mapPos.top > (-1)*height && (mapPos.bottom > 15  && mapPos.bottom < 55) ){
			mapLock.classList.add('lock-map-absolute');
			mapLock.classList.remove('lock-map-fixed');
			console.log(4);	/*randomSvg();
	function randomSvg(){
		var beginning = 'url("data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg">';
		var circles;
		for(var i=0;i<4;i++){
			circles += '<circle cx="'+ Math.floor((Math.random() * 1000) + 1) +'" cy="'+ Math.floor((Math.random() * 600) + 1) +'" r="'+ Math.floor((Math.random() * 8) + 1) +'" stroke="none" fill="#E63244"/>';
		}
		var end = '</svg>';
		$('#education').css('background-image', (beginning + circles + end));
		console.log(beginning + circles + end);
	}*/
		}
	}



	function generateDots(){
		//d3.select(#education).selectAll().enter().append('circle').
	}

	// FUNCTION FUNCTIONS ---------------------

	// Set element to screen height
	function setToScreenHeight(ele){
		var height = window.innerHeight;
		ele.style.height = (height) + 'px';
	}
	function hasClass(element, cls) {
			return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
	}


	function getBrowserSize() {
		var dimensions = [];
		if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		dimensions.width = window.innerWidth;
		dimensions.height = window.innerHeight;
		} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		dimensions.width = document.documentElement.clientWidth;
		dimensions.height = document.documentElement.clientHeight;
		} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		dimensions.width = document.body.clientWidth;
		dimensions.height = document.body.clientHeight;
		}
		return dimensions;
	}

});
