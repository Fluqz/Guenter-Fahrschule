var gulp = require('gulp');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var gulpUtil = require('gulp-util');
var uglify = require('gulp-uglify');


gulp.task('browser-sync', function(){
	browserSync.init({
		server: { baseDir: './app/' }
	});

	gulp.watch('./app/*.html').on('change', browserSync.reload);
	gulp.watch('./app/css/main.css', ['css']);
	gulp.watch('./app/css/responsive.css', ['responsive']);
	gulp.watch('./app/js/main.js', ['uglify']);
});

gulp.task('css', function(){
	gulp.src('./app/css/main.css')
	.pipe(cssmin())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('./app/css/'))
	.pipe(browserSync.stream());
});

gulp.task('responsive', function(){
	gulp.src('./app/css/responsive.css')
	.pipe(cssmin())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('./app/css/'))
	.pipe(browserSync.stream());
});

gulp.task('uglify', function(){
	gulp.src('./app/js/main.js')
	.pipe(uglify().on('error', gulpUtil.log))
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('./app/js/'))
	.pipe(browserSync.stream());
});

gulp.task('default', ['browser-sync', 'css', 'responsive', 'uglify']);
